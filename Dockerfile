FROM ezlabgva/busco:v5.7.1_cv1

#----------------------------------------------------------
# METADATA
#----------------------------------------------------------
LABEL base.image="ezlabgva/busco:v5.7.1_cv1"
LABEL version="v3.5.0"
LABEL software="OrthoLoger"
LABEL software.version="3.5.0"
LABEL about.summary="a pipeline for finding Orthologs, including BUSCO for mapping"
LABEL about.home="https://www.orthodb.org/"
LABEL about.documentation="https://orthologer.ezlab.org"
LABEL license="GPL:v3"
LABEL about.tags="genomics, transcriptomics, bioinformatics, evolution"

MAINTAINER Fredrik Tegenfeldt <fredrik.tegenfeldt@unige.ch>

USER root

ARG DOCKER_TAG
ENV APP_VERSION=$DOCKER_TAG

ENV PKG_ORTHOLOGER_VERSION=3.5.0
ENV ORTHOLOGER_VERSION=3.5.0

ENV BRHCLUS_VERSION=5.2.5
ENV DIR_ORTHOLOGER=/usr/local/ORTHOLOGER-${ORTHOLOGER_VERSION}
ENV PKG_ORTHOLOGER_DIR=orthologer_${PKG_ORTHOLOGER_VERSION}/ORTHOLOGER-${ORTHOLOGER_VERSION}
ENV ODBMAPPER_WORK=/odbwork

#----------------------------------------------------------
# START BUILD
#----------------------------------------------------------
WORKDIR /

RUN export LD_LIBRARY_PATH=/lib/x86_64-linux-gnu && apt-get update && apt-get install software-properties-common python3-pip git wget curl rsync build-essential cmake bc ncbi-blast+ -y
RUN python3 -m pip install numpy ete3

# Create odbpipe user and add to busco_users
RUN groupadd -r docker && useradd -r -g docker odbpipe && usermod -a -G busco_users odbpipe && mkdir /packages && chown odbpipe:root /packages && chmod 775 /packages

###&& mkdir /odbwork && chown odbpipe:busco_users /odbwork && chmod 775 /odbwork

# get orthodb software
WORKDIR /packages
###RUN date +'%s' > random_bytes &&
RUN wget https://data.orthodb.org/current/download/software/orthologer_${PKG_ORTHOLOGER_VERSION}.tgz && tar -xzf orthologer_${PKG_ORTHOLOGER_VERSION}.tgz && ls orthologer_${PKG_ORTHOLOGER_VERSION}/BRHCLUS-${BRHCLUS_VERSION} 2>&1 && ls orthologer_${PKG_ORTHOLOGER_VERSION}/ORTHOLOGER-${ORTHOLOGER_VERSION} 2>&1 /dev/null

# install orthologer, mmseqs2, diamond and cdhit
WORKDIR /packages/orthologer_${PKG_ORTHOLOGER_VERSION}
###RUN date +'%s' > .random_bytes &&
RUN /bin/bash -c "PREFIX=/usr/local ./install_pkg.sh && chmod +x /usr/local/bin/orthologer /usr/local/bin/orthomapper"

# copy docs
WORKDIR /packages/orthologer_${PKG_ORTHOLOGER_VERSION}/ORTHOLOGER-${ORTHOLOGER_VERSION}
RUN mkdir -p ${DIR_PIPELINE}/docs && cp -vf docs/mapping.txt docs/create_project.txt docs/run_project.txt ${DIR_PIPELINE}/docs/

# copy special scripts
COPY scripts/docker_introduction.sh /usr/bin/introduction 
COPY scripts/docker_run_tests.sh /usr/bin/run_tests

USER odbpipe

WORKDIR /odbwork

CMD ["introduction"]

