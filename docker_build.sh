#!/bin/bash
export DOCKER_TAG=$(grep "^ENV PKG_ORTHOLOGER_VERSION=" Dockerfile | cut -f 2 -d '=')
[[ "${DOCKER_TAG:1}" != "v" ]] && DOCKER_TAG="v$DOCKER_TAG"

echo
echo "**** This will build the docker image ****"
echo "Tag: $DOCKER_TAG"
echo

declare -u answer
answer=${1-}
if [[ -z $answer ]]; then
    echo -n "Clear previous build cache? (Y/N) "
    read answer
fi

nocache=""
if [[ "$answer" == "Y" ]]; then
    nocache="--no-cache"
    echo "will clear cache"
else
    echo "will keep previous cache"
fi

docker build ./ --build-arg DOCKER_TAG=$DOCKER_TAG -t orthologer:$DOCKER_TAG $nocache
