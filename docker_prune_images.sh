#!/bin/bash
echo
echo "**** This will prune all non-active docker images ****"
echo
declare -u answer
echo -n "Are you sure? (Y/N) "
read answer
[[ $answer == "Y" ]] || { echo "aborting..."; exit 0; }
echo -n "Are you REALLY sure? (Y/N) "
read answer
[[ $answer == "Y" ]] || { echo "aborting..."; exit 0; }
docker system prune -a -f
