#!/bin/bash
[[ -d cdhit ]] || { echo "error: missing cdhit directory - is it cloned?"; exit 255; }
cp Makefile cdhit/Makefile
cd cdhit
make -j 4
