#!/bin/bash
[[ -d MMseqs2 ]] || { echo "error: missing MMseqs2 directory - is it cloned?"; exit 255; }

mkdir -p MMseqs2/build
cd MMseqs2/build
cmake -DNATIVE_ARCH=0 -DCMAKE_CXX_FLAGS="-march=x86-64 -mtune=generic" ../

make -j 4
