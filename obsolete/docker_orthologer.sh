#!/bin/bash

intro() {
    echo
    echo "----------------------------------------------------------------"
    echo "                     OrthoLoger Pipeline                        "
    echo "----------------------------------------------------------------"
    echo
    echo " This a Docker container for the OrthoLoger pipeline.   "
    echo
    echo " This will take a while as it downloads the image and installs  "
    echo " the required packages.                                         "
    echo
    echo " To setup a running using a local storage, proceed as follows:  "
    echo
    echo " - create an empty directory (e.g odb)     "
    echo "   > mkdir odb"
    echo
    echo " - run setup using (assuming tagged as 'orthologer:latest')     "
    echo "   > docker run -u \$(id -u) -v \$(pwd)/odb:/odbwork orthologer:latest orthologer -c create"
    echo
    echo " - import fasta files"
    echo "   this step requires that /odbdata is mounted in the image"
    echo "   > docker run -u \$(id -u) -v \$(pwd)/odb:/odbwork -v <data>:/odbdata orthologer:latest orthologer -c import"
    echo
    echo " - run orthologer"
    echo "   > docker run -u \$(id -u) -v \$(pwd)/odb:/odbwork orthologer:latest orthologer -c run"
    echo
    echo " - alternatively, run all above "
    echo "   > docker run -u \$(id -u) -v \$(pwd)/odb:/odbwork -d <data>:/odbdata orthologer:latest orthologer -c go"
    echo
    echo " - in order to run scripts directly in /odbwork:"
    echo "   > docker run -u \$(id -u) -v \$(pwd)/odb:/odbwork orthologer:latest ./orthologer.sh <args>"
    echo
    echo " Once setup is complete, all parameters are set in orthologer_conf.sh .  "
    echo " Review these settings, in particular wrt nr of cores available,"
    echo " if a computing cluster is available, etc.                      "
    echo " The most time consuming part is usually the ALIGNMENT step.            "
    echo
    echo " Further help is available running: ./orthologer.sh -h                  "
    echo " In addition, there are two files with instructions in ./docs   "
    echo
}

usage() {
    echo "usage: $0 [-h -d <data dir> -p <project name> -c <command>] | [cmdline]"
    echo
    echo "  -h               : this message"
    echo "  -d <data dir>    : location of fasta files, by default $ORTHOLOGER_DATA"
    echo "  -w <work dir>    : work dir, by default $ORTHOLOGER_WORK"
    echo "  -p               : project name, default = $ORTHOLOGER_NAME"
    echo "  -c               : what to do, can be one of"
    echo "                     create       create orthologer environment"
    echo "                     import       import fasta files"
    echo "                     setup        create+import"
    echo "                     run          run the orthologer"
    echo "                     go           all of the above"
    echo
    exit 0
}

# these should be fixed
ORTHOLOGER_DATA=/odbdata
ORTHOLOGER_WORK=$(pwd)

# this maybe set by user
ORTHOLOGER_NAME=odbproj

process_args() {
    local opts="hd:w:p:t:c:w:"
    local OPTIND=1
    local OPTS=""
    local nreq=0
    while getopts $opts OPT; do
        case "$OPT" in
            h) usage
               exit 0
               ;;
            d) ORTHOLOGER_DATA="$OPTARG"
               ;;
            w) ORTHOLOGER_WORK="$OPTARG"
               ;;
            p) ORTHOLOGER_NAME="$OPTARG"
               ;;
            c) COMMAND="$OPTARG"
               ;;
        esac
    done
    shift $((OPTIND-1))

    # check command
    COMMAND="${COMMAND,,}"
    local optsreq=0
    if [[ ${COMMAND} == run ]]; then
        DO_RUN=1
        
    elif [[ ${COMMAND} == create ]]; then
        DO_CREATE=1

    elif [[ ${COMMAND} == import ]]; then
        DO_IMPORT=1

    elif [[ ${COMMAND} == setup ]]; then
        DO_CREATE=1
        DO_IMPORT=1
        
    elif [[ ${COMMAND} == go ]]; then
        DO_CREATE=1
        DO_IMPORT=1
        DO_RUN=1
        
    elif [[ -n $COMMAND ]]; then
        echo "error: invalid command $COMMAND"
        return 255
    fi
    
    return 0
}

# process arguments and show usage if none are given
[[ $# -eq 0 ]] && { intro; exit 0; }
process_args "$@" || exit 255

if [[ -z $COMMAND ]]; then
    intro
    exit 0
fi

# check existence of directories
[[ -d $ORTHOLOGER_WORK ]] || { echo "error: missing orthologer work dir at $ORTHOLOGER_WORK - is it mounted?"; exit 255; }

echo "info: moving to work directory $ORTHOLOGER_WORK"
cd $ORTHOLOGER_WORK || { echo "error: missing work directory at $ORTHOLOGER_WORK"; exit 255; }

if (( DO_IMPORT )); then
    # make sure it contains a trailing /
    ORTHOLOGER_DATA="${ORTHOLOGER_DATA}/"
    ORTHOLOGER_DATA="${ORTHOLOGER_DATA//\/\//\/}"
    echo "info: import data from $ORTHOLOGER_DATA"
    [[ -d $ORTHOLOGER_DATA ]] || { echo "error: missing orthologer data dir at $ORTHOLOGER_DATA - is it mounted?"; exit 255; }
fi

# check existance of some commands
which setup_odb.sh >& /dev/null || { echo "error: missing setup_odb.sh - possibly not running in a docker image?"; exit 255; }
which orthologer   >& /dev/null || { echo "error: missing orthologer - possibly not running in a docker image?"; exit 255; }

#-----------------------------------------------
setup_environment() {
    echo
    echo "--- setup project environment at $WORKDIR"
    echo
    local nfiles=$(find $ORTHOLOGER_WORK/ -type f | wc -l)
    if (( nfiles == 0 )); then
        # create empty environment
        cd $ORTHOLOGER_WORK && setup_odb.sh
    else
        if [[ -f $ORTHOLOGER_WORK/orthologer.sh ]] && [[ -f $ORTHOLOGER_WORK/orthologer_conf.sh ]] && [[ -d $ORTHOLOGER_WORK/PWC ]]; then
            echo "info: work directory environment already exists"
        else
            echo "error: non empty work directory is not an orthologer work environment"
            return 255
        fi
    fi
    return 0
}

#-----------------------------------------------
import_fasta() {
    echo
    echo "--- import fasta from $ORTHOLOGER_DATA"
    echo
    local nfiles=$(find $ORTHOLOGER_DATA -type f | wc -l)
    if (( nfiles == 0 )); then
        echo "error: given data directory seems to be empty"
        return 255
    fi
    cd $ORTHOLOGER_WORK
    if [[ -f "${ORTHOLOGER_NAME}.txt" ]]; then
        echo "    import file ${ORTHOLOGER_NAME}.txt already exists"
    else
        echo "    build import file ${ORTHOLOGER_NAME}.txt"
        # build import file from user given fasta files
        build_import_file.sh -o ${ORTHOLOGER_NAME}.txt $ORTHOLOGER_DATA || { echo "error: unable to build import file"; return 255;}
    fi
    
    # import them into this project
    echo "    import ${ORTHOLOGER_NAME}.txt"
    ./orthologer.sh manage -f ${ORTHOLOGER_NAME}.txt || { echo "error: unable to import fasta files"; return 255;}

    # update orthologer_conf.sh such that it includes the new todo file
    echo "    update orthologer_conf.sh"
    ./orthologer.sh -t todo/${ORTHOLOGER_NAME}.todo -u || { echo "error: unable to update orthologer_conf.sh"; return 255;}

    return 0
}

#-----------------------------------------------
run_orthologer() {
    echo
    echo "--- run orthologer"
    echo
    cd $ORTHOLOGER_WORK
    local ec=0
    T0=$(date +%s)
    ./orthologer.sh -t todo/${ORTHOLOGER_NAME}.todo -r all
    ec=$?
    T1=$(date +%s)
    dT=$((T1-T0))
    echo
    echo "--------------------------------------"
    echo " Run finished in $dT seconds."
    [[ $ec -eq 0 ]] || echo " Error code = $ec"
    echo " Final results in 'Results' folder."
    echo "--------------------------------------"
    echo
}

#-----------------------------------------------
#RUNLOG=${ORTHOLOGER_WORK}run_$(date +%y%m%d%H%M%S).log
echo
echo "--- Start run" #, log file at $RUNLOG"
echo
{
    # first setup environment in workdir
    if (( DO_CREATE )); then
        setup_environment          || exit 255
    fi
    # import data and prepare for run
    if (( DO_IMPORT )); then
        import_fasta               || exit 255
    fi
    # run pipeline
    if (( DO_RUN )); then
        run_orthologer             || exit 255
    fi
} ###>& $RUNLOG

echo
echo "--- DONE ---"
echo
