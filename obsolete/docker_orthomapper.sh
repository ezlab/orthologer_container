#!/bin/bash

intro() {
    echo
    echo "----------------------------------------------------------------"
    echo "                     OrthoLoger Mapper                          "
    echo "----------------------------------------------------------------"
    echo
    echo " To setup a running using a local storage, proceed as follows:  "
    echo
    echo " - create an empty directory (e.g odb)     "
    echo "   > mkdir odb"
    echo
#    echo " - run setup using (assuming tagged as 'orthologer:latest')     "
#    echo "   > docker run -u \$(id -u) -v \$(pwd)/odb:/odbwork orthologer:latest orthomapper -c create"
#    echo
    echo " - copy the fasta file(s) to be mapped to ./odb (or a sub directory)"
    echo
    echo " - if you want to create environment without running something:"
    echo "   > docker run -u \$(id -u) -v \$(pwd)/odb:/odbwork orthologer:latest orthomapper -c create"
    echo
    echo " - run mapper (will also create environment if not already created)"
    echo "   > docker run -u \$(id -u) -v \$(pwd)/odb:/odbwork orthologer:latest orthomapper -c run -p <label> -f <fasta file> [-n <OrthoDB node>]"
    echo
    echo "   The fasta file is given with a path accessible under /odbwork."
    echo
    echo "   Option '-n' provides an optional target OrthoDB node id as a NCBI taxid."
    echo "   For example mapping against primates the option would be '-n 9443'."
    echo
    echo " - in order to run the orthomapper scripts directly in /odbwork:"
    echo "   > docker run -u \$(id -u) -v \$(pwd)/odb:/odbwork orthologer:latest ./orthomapper.sh <cmd> <label> ..."
    echo
    echo " Use 'orthomapper -c HELP' to see what additional commands <cmd> are available."
    echo " The <label> is the one defined with option '-l' above."
    echo
}

usage() {
    echo "usage: $0 [-p <project label> -c <command> -f <fasta file> [-n <OrthoDB node id>]]"
    echo
    echo "  -h               : help on commands for -c option (same as calling ./orthomapper.sh help)"
    echo "                   : if environment is not initialised it will return this msg"
    echo "  -p <label>       : project label (required with -c)"
    echo "  -c <cmd>         : what to do, can be one of"
    echo "                   : create       create orthologer environment"
    echo "                   : run          possibly create and run the mapper"
    echo "                   : commands provided by -h"
    echo "  -f <fasta>       : fasta file to be mapped"
    echo "  -n <OrthDB node> : optional target OrthoDB node id (NCBI taxid)"
    echo "                     if not given, it will be estimated using BUSCO"
    echo "  -w <work dir>    : work dir, by default $MAPPER_WORK"
    echo
    exit 0
}

help_on_commands() {
    if [[ -d $MAPPER_WORK ]]; then
        cd $MAPPER_WORK
        if [[ -x orthomapper.sh ]]; then
            echo "--------------------------------"
            echo "     Help on orthomapper.sh"
            echo "--------------------------------"
            echo
            ./orthomapper.sh help
            return 0
        fi
    fi
    echo "info: environment is not yet initialised - no help on orthomapper.sh commands"
    usage
}

# these should be fixed
MAPPER_WORK=$(pwd)

# check existance of some commands
which setup_odb.sh >& /dev/null || { echo "error: missing setup_odb.sh - possibly not running in a docker image?"; exit 255; }
which orthomapper  >& /dev/null || { echo "error: missing orthomapper.sh - environment not yet initialised";       exit 255; }


# this must be set by user
MAPPER_NAME=
COMMAND=
INPUT_FASTA=
INPUT_NODEID=

DO_CMD=0
DO_HELP=0
DO_RUN=0
DO_CREATE=0
process_args() {
    MAPPER_NAME=
    COMMAND=
    INPUT_FASTA=
    INPUT_NODEID=

    local opts="hd:w:p:c:f:n:"
    local OPTIND=1
    local OPTS=""
    local nreq=0
    while getopts $opts OPT; do
        case "$OPT" in
            h) DO_HELP=1
               ;;
            w) MAPPER_WORK="$OPTARG"
               ;;
            p) MAPPER_NAME="$OPTARG"
               ;;
            c) COMMAND="$OPTARG"
               ;;
            f) INPUT_FASTA="$OPTARG"
               ;;
            n) INPUT_NODEID="$OPTARG"
               ;;
        esac
    done
    shift $((OPTIND-1))

    # check command
    COMMAND="${COMMAND,,}"
    local optsreq=0
    local reqlabel=0
    if [[ ${COMMAND} == run ]]; then
        DO_RUN=1
        DO_CREATE=1
        reqlabel=1
        
    elif [[ ${COMMAND} == create ]]; then
        DO_CREATE=1
        reqlabel=0

    elif [[ ${COMMAND} == map ]]; then
        echo "error: use 'run' instead of 'map'"
        return 255
        
    elif [[ -n $COMMAND ]]; then
        (( DO_HELP == 1 )) && return 0
        DO_CMD=1
        reqlabel=0
        #echo "error: invalid command $COMMAND"
        #return 255
    fi

    if [[ -z $MAPPER_NAME ]]; then
        if (( reqlabel == 1 )); then
            echo "error: option -p <project label> is required"
            return 255
        fi
    fi
    

    return 0
}

# process arguments and show usage if none are given
[[ $# -eq 0 ]] && { intro; exit 0; }
process_args "$@" || exit 255

if (( DO_HELP == 1 )); then
    help_on_commands
    exit 0
fi

if [[ -z $COMMAND ]]; then
    intro
    exit 0
fi

# check existence of directories
[[ -d $MAPPER_WORK ]] || { echo "error: missing orthomapper work dir at $MAPPER_WORK - is it mounted?"; exit 255; }

#echo "info: moving to work directory $MAPPER_WORK"
cd $MAPPER_WORK || { echo "error: missing work directory at $MAPPER_WORK"; exit 255; }

# check existance of some commands
which setup_odb.sh >& /dev/null || { echo "error: missing setup_odb.sh - possibly not running in a docker image?"; exit 255; }
which orthomapper  >& /dev/null || { echo "error: missing orthologer - possibly not running in a docker image?";   exit 255; }

#-----------------------------------------------
#setup_environment() {
#    echo
#    echo "--- setup project environment at $MAPPER_WORK"
#    echo
#    local nfiles=$(find $MAPPER_WORK/ -type f | wc -l)
#    if (( nfiles == 0 )); then
#        # create mapper environment
#        cd $MAPPER_WORK && setup_odb.sh
#        cd -
#    else
##        if [[ -f $MAPPER_WORK/orthomapper.sh ]] && [[ -f $MAPPER_WORK/orthomapper_conf.sh ]]; then
#            echo "info: work directory environment already exists"
#        else
#            echo "error: non empty work directory is not an orthomapper work environment"
#            return 255
#        fi
#    fi
#    return 0
#}

setup_environment() {
    echo
    echo "--- setup project environment at $MAPPER_WORK"
    echo
    if [[ -f $MAPPER_WORK/orthomapper.sh ]] && [[ -f $MAPPER_WORK/orthomapper_conf.sh ]]; then
        echo "info: work directory environment already exists"
    else
        echo "info: creating work directory environment"
        cd $MAPPER_WORK && setup_odb.sh
        cd -
    fi
    return 0
}


#-----------------------------------------------
run_orthomapper() {
    echo
    echo "--- run orthomapper"
    echo
    cd $MAPPER_WORK
    [[ -x orthomapper.sh ]] || {
        echo "error: missing orthomapper.sh in workdir"
        return 255
    }
    local ec=0
    T0=$(date +%s)
    ./orthomapper.sh MAP $MAPPER_NAME $INPUT_FASTA $INPUT_NODEID
    ec=$?
    T1=$(date +%s)
    dT=$((T1-T0))
    echo
    echo "--------------------------------------"
    echo " Run finished in $dT seconds."
    [[ $ec -eq 0 ]] || echo " Error code = $ec"
    echo " Final results in 'Results' folder."
    echo "--------------------------------------"
    echo
}

#-----------------------------------------------
run_orthomapper_cmd() {
    echo
    echo "--- run orthomapper command $COMMAND"
    echo
    cd $MAPPER_WORK
    [[ -x orthomapper.sh ]] || {
        echo "error: missing orthomapper.sh in workdir"
        return 255
    }
    local ec=0
    T0=$(date +%s)
    ./orthomapper.sh $COMMAND $MAPPER_NAME
    ec=$?
    T1=$(date +%s)
    dT=$((T1-T0))
    echo
    echo "--------------------------------------"
    echo " Command finished in $dT seconds."
    [[ $ec -eq 0 ]] || echo " Error code = $ec"
    echo "--------------------------------------"
    echo
}

#-----------------------------------------------
echo
echo "--- Start run" #, log file at $RUNLOG"
echo
{
    # first setup environment in workdir
    if (( DO_CREATE )); then
        setup_environment    || exit 255
    fi
    # run pipeline
    if (( DO_RUN )); then
        run_orthomapper      || exit 255
    fi

    if (( DO_CMD )); then
        run_orthomapper_cmd  || exit 255
    fi
} ###>& $RUNLOG

echo
echo "--- DONE ---"
echo
