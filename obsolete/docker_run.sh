#!/bin/bash
echo "OBSOLETE - use the command 'orthologer'"
exit 255

#hd:w:p:t:c:
usage() {
    echo "usage: $0 -w <work dir> [-h -d <data dir> -p <project name> -t <docker tag> -c <command>] | [cmdline]"
    echo
    echo "  -h               : this message"
    echo "  -w               : work directory - empty or containing a previously setup orthologer environment"
    echo "  -d               : data directory - contains ONLY the fasta files to be used"
    echo "  -t               : docker tag, default = $DOCKER_TAG"
    echo "  -p               : project name, default = $PROJECT_NAME"
    echo "  -n               : newick tree file (import)"
    echo "  -c               : what to do, can be one of"
    echo "                     setup        create orthologer environment (requires -w)"
    echo "                     import       import fasta files (requires -w,-d and -p)"
    echo "                                  if tree is given (-n), it is imported and common.sh is updated"
    echo "                     run          run the orthologer (requires -w)"
    echo "                     go           setup, import and run the orthologer"
    echo
    exit 0
}
#-----------------------------------------------
# set defaults

# directories inside the docker run
DOCKER_HOME=/odbwork
DOCKER_DATA=/odbdata

# project name
PROJECT_NAME=odbproj

# docker tag
DOCKER_TAG="ezlabgva/orthologer:v3.2.2"

#-----------------------------------------------
# process args

# variables set by args

# system data directory
DATADIR=""

# file labels
FILELBLS=""

# system work directory
WORKDIR=""

# command
COMMAND=""
CMDLINE=""
TREE=""

DO_SETUP=0
DO_IMPORT=0
DO_RUN=0
process_args() {
    local opts="hd:w:p:t:c:l:n:"
    local OPTIND=1
    local OPTS=""
    local nreq=0
    while getopts $opts OPT; do
        case "$OPT" in
            h) usage
               exit 0
               ;;
            d) DATADIR="$OPTARG"
               ;;
            w) WORKDIR="$OPTARG"
               nreq=$((nreq+1))
               ;;
            p) PROJECT_NAME="$OPTARG"
               ;;
            t) DOCKER_TAG="$OPTARG"
               ;;
            c) COMMAND="$OPTARG"
               ;;
            l) FILELBLS="$OPTARG"
               ;;
            n) TREE="$OPTARG"
               ;;
        esac
    done
    shift $((OPTIND-1))

    # get optional remaining cmd line
    CMDLINE="$@"
    
    # check command
    COMMAND="${COMMAND,,}"
    local optsreq=0
    if [[ ${COMMAND} == run ]]; then
        optsreq=0
        DO_RUN=1
        
    elif [[ ${COMMAND} == setup ]]; then
        optsreq=0
        DO_SETUP=1
        
    elif [[ ${COMMAND} == import ]]; then
        optsreq=1
        DO_IMPORT=1
        
    elif [[ ${COMMAND} == go ]]; then
        optsreq=1
        DO_IMPORT=1
        DO_SETUP=1
        DO_RUN=1
        
    elif [[ -n $COMMAND ]]; then
        echo "error: invalid command $COMMAND"
        return 255
    fi
    
    # clear command line if command is given
    [[ -z $COMMAND ]] || CMDLINE=""
    
    # check required options
    if   (( optsreq == 0 )); then
        [[ -n $WORKDIR ]] || { echo "error: missing -w option"; return 255; }
    elif (( optsreq == 1 )); then
        [[ -n $WORKDIR      ]] || { echo "error: missing -w option"; return 255; }
        [[ -n $DATADIR      ]] || { echo "error: missing -d option"; return 255; }
        [[ -n $PROJECT_NAME ]] || { echo "error: missing -p option"; return 255; }
    fi
    return 0
}

[[ $# -eq 0 ]] && { usage; exit 0; }
process_args "$@" || exit 255

#-----------------------------------------------
# create workdir if it does not exist
mkdir -p "$WORKDIR" || { echo "error: failed creating work directory"; exit 255; }

# check directories
[[ -d $WORKDIR ]] || { echo "error: missing target directory at $WORKDIR"; exit 255; }
[[ -d $DATADIR ]] || { echo "error: missing data directory at $DATADIR";   exit 255; }

#-----------------------------------------------
# check if docker exists and set DOCKER_HOME
DOCKER_CMD=$(which docker)
[[ -x $DOCKER_CMD ]] || { echo "error: no docker command available!"; exit 255; }

# is it running?
$DOCKER_CMD info >& /dev/null || { echo "error: unable to run docker - is the server running?"; exit 255; }

# does the tag exist?
hashtag=$($DOCKER_CMD images -q $DOCKER_TAG)
[[ -n $hashtag ]] || { echo "error: docker cannot find tag $DOCKER_TAG"; exit 255; }

# set work dir by simply calling pwd
DOCKER_HOME=$($DOCKER_CMD run -u $(id -u) $DOCKER_TAG pwd)


#-----------------------------------------------
echo
echo "------------------------------------------"
echo "Docker tag : $DOCKER_TAG"
echo "Project    : $PROJECT_NAME"
echo "Work dir   : $WORKDIR"
echo "Data dir   : $DATADIR"
echo "Command    : $COMMAND"
echo "------------------------------------------"
echo

WORKDIR=$(cd $WORKDIR && pwd)
DATADIR=$(cd $DATADIR && pwd)

#-----------------------------------------------
file_setvar() {
    [[ $# -eq 3 ]] || return 255
    local fscript="$1"
    [[ -f $fscript ]] || return 255
    
    local var="$2"
    local value=""
    value=$(echo "$3" | sed 's/\//\\\//g') || return 255
    # ugly!!!!
    sed -i 's/^\(\s*\)'"${var}="'.*/'"${var}=${value}"'/' "$fscript"
    sed -i 's/^\(\s*\)'"export ${var}="'.*/'"export ${var}=${value}"'/' "$fscript"
    return 0
}

#-----------------------------------------------
setup_environment() {
    echo
    echo "--- setup project environment at $WORKDIR"
    echo
    local nfiles=$(ls $WORKDIR/ | wc -l)
    if (( nfiles == 0 )); then
        # create empty environment
        echo "info: create empty environment"
        echo "info: cmd = $DOCKER_CMD run -u $(id -u) -v ${WORKDIR}:${DOCKER_HOME} $DOCKER_TAG setup_odb.sh"
        $DOCKER_CMD run -u $(id -u) -v ${WORKDIR}:${DOCKER_HOME} $DOCKER_TAG setup_odb.sh
    else
        if [[ -f $WORKDIR/orthologer.sh ]] && [[ -f $WORKDIR/common.sh ]] && [[ -d $WORKDIR/PWC ]]; then
            echo "info: work directory environment already exists"
        else
            if  [[ -f $WORKDIR/orthologer.sh ]] && [[ -d $WORKDIR/PWC ]]; then
                # just missing common.sh ! recreate it
                echo "missing common.sh - will attempt to recreate it"
                # create a dummy common.sh, will be rebuilt with ./orthologer.sh -u
                cat <<EOF > common.sh
#!/bin/bash
export DIR_PIPELINE=\$(readlink ./sbin)/../
[[ -f "\${DIR_PIPELINE}/bin/common_odb.sh" ]] || { echo "ERROR unable to read system common_odb.sh : \$DIR_PIPELINE/bin/common_odb.sh" 1>&2; exit 255; }
source "\${DIR_PIPELINE}/bin/common_odb.sh"
EOF
                
                $DOCKER_CMD run -u $(id -u) -v ${WORKDIR}:${DOCKER_HOME} -v ${DATADIR}:${DOCKER_DATA} $DOCKER_TAG \
                            /bin/bash -c \
                            "build_import_file.sh -o ${PROJECT_NAME}.txt $DOCKER_DATA && ./orthologer.sh manage -f ${PROJECT_NAME}.txt && ./orthologer.sh -t todo/${PROJECT_NAME}.todo -u"
            fi
            if [[ ! -f $WORKDIR/common.sh ]]; then
                echo "error: non empty work directory is not an orthologer work environment"
                return 255
            fi
        fi
    fi
    return 0
}

#-----------------------------------------------
import_tree() {
    [[ -n "$TREE" ]] || return 0
    echo
    echo "--- import tree from file $TREE"
    echo
    [[ -f "$TREE" ]] || { echo "error: missing tree file at $TREE"; return 255; }
    [[ -d ${WORKDIR}/todo ]] || { echo "error: seems like pipeline is not properly setup (missing todo) at $WORKDIR"; return 255; }
    [[ -f ${WORKDIR}/common.sh ]] || { echo "error: seems like pipeline is not properly setup (missing common.sh) at $WORKDIR"; return 255; }
    
    cp -f "$TREE" "${WORKDIR}/todo" || { echo "error: failed to copy tree file $TREE to ${WORKDIR}/todo"; return 255; }
    file_setvar "${WORKDIR}/common.sh" TREE_ENABLED "1" || { echo "error: failed to set variable TREE_ENABLED in ${WORKDIR}/common.sh"; return 255; }
    file_setvar "${WORKDIR}/common.sh" TREE_INPUT_FILE "todo/$(basename $TREE)" || { echo "error: failed to set variable TREE_INPUT_FILE in ${WORKDIR}/common.sh"; return 255; }
    return 0
}

#-----------------------------------------------
import_fasta() {
    echo
    echo "--- import fasta from $DATADIR"
    echo
    local nfiles=$(ls $DATADIR/ | wc -l)
    if (( nfiles == 0 )); then
        echo "error: given data directory seems to be empty"
        return 255
    fi
    echo "info: import fasta"
    echo "$DOCKER_CMD run -u $(id -u) -v ${WORKDIR}:${DOCKER_HOME} -v ${DATADIR}:${DOCKER_DATA} $DOCKER_TAG \
           /bin/bash -c \
           \"build_import_file.sh -o ${PROJECT_NAME}.txt $DOCKER_DATA && ./orthologer.sh manage -f ${PROJECT_NAME}.txt && ./orthologer.sh -t todo/${PROJECT_NAME}.todo -u\""
    
    $DOCKER_CMD run -u $(id -u) -v ${WORKDIR}:${DOCKER_HOME} -v ${DATADIR}:${DOCKER_DATA} $DOCKER_TAG \
           /bin/bash -c \
           "build_import_file.sh -o ${PROJECT_NAME}.txt $DOCKER_DATA && ./orthologer.sh manage -f ${PROJECT_NAME}.txt && ./orthologer.sh -t todo/${PROJECT_NAME}.todo -u"
    [[ $? -eq 0 ]] || { echo "error: failed importing fasta"; return 255; }
    return 0
}

#-----------------------------------------------
run_orthologer() {
    echo
    echo "--- run orthologer"
    echo
    local ec=0
    echo "info: run orthologer - all steps"
    echo "info: cmd = $DOCKER_CMD run -u $(id -u) -v ${WORKDIR}:${DOCKER_HOME} $DOCKER_TAG ./orthologer.sh -r all"
    T0=$(date +%s)
    $DOCKER_CMD run -u $(id -u) -v ${WORKDIR}:${DOCKER_HOME} $DOCKER_TAG ./orthologer.sh -r all
    ec=$?
    T1=$(date +%s)
    dT=$((T1-T0))
    echo
    echo "--------------------------------------"
    echo " Run finished in $dT seconds."
    [[ $ec -eq 0 ]] || echo " Error code = $ec"
    echo " Final results in 'Results' folder."
    echo "--------------------------------------"
    echo

}

#-----------------------------------------------


if [[ -n $CMDLINE ]]; then
    $DOCKER_CMD run -u $(id -u) -v ${WORKDIR}:${DOCKER_HOME} -v ${DATADIR}:${DOCKER_DATA} $DOCKER_TAG \
                /bin/bash -c "$CMDLINE"
    exit 0
fi

RUNLOG=${WORKDIR}run_$(date +%y%m%d%H%M%S).log
echo
echo "--- Start run, log file at $RUNLOG"
echo
{
    # first setup environment in workdir
    if (( DO_SETUP )); then
        setup_environment          || exit 255
    fi
    # import data and prepare for run
    if (( DO_IMPORT )); then
        import_tree                || exit 255
        import_fasta               || exit 255
    fi
    # run pipeline
    if (( DO_RUN )); then
        run_orthologer             || exit 255
    fi
} >& $RUNLOG

echo
echo "--- DONE ---"
echo
