#!/bin/bash
# get orthologer version
source /usr/local/orthologer/bin/version.sh
echo
echo "----------------------------------------------------------------"
echo "                     OrthoLoger/ODB-mapper                      "
echo "----------------------------------------------------------------"
echo
echo " Version: $OP_VERSION"
echo
echo " This a Docker container for the OrthoLoger pipeline and the corresponding mapping tool."
echo
echo " It runs in two modes:  "
echo " 1. orthologer      - find orthologs among a set of input fasta files"
echo " 2. ODB-mapper      - map a given fasta file against OrthoDB orthologs"
echo
echo " In both cases, start by creating an empty directory (e.g 'odb')."
echo " This directory should then be mounted against /odbwork in the container."
echo
echo " For further instructions call either 'orthologer' or 'ODB-mapper' as follows"
echo " > docker run -u \$(id -u) -v \$(pwd)/odb:/odbwork orthologer:v$ORTHOLOGER_VERSION orthologer"
echo "   OR "
echo " > docker run -u \$(id -u) -v \$(pwd)/odb:/odbwork orthologer:v$ORTHOLOGER_VERSION ODB-mapper"
echo
echo " ODB-mapper will run against the current release of OrthoDB."
echo " If a specific version is required add a version string to the command:"
echo " > ODB-mapper_v11     -> will use OrthoDB v11 api."
echo
echo " Allowed versions are 'v11' and 'v12'."
echo
echo " Optionally run tests using"
echo " > docker run -u \$(id -u) -v \$(pwd)/odb:/odbwork orthologer:v$ORTHOLOGER_VERSION run_tests"
echo
echo " Note that running the tests will write in the given work directory, i.e in ./odb ."

