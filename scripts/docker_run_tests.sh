#!/bin/bash

if [[ ! -d ./test ]]; then
    echo "copy orthologer test directory to $(pwd)"
    cp -Rf /usr/local/orthologer/test ./
fi

cd test || { echo "error: unable to move to directory $(pwd)/test"; exit 255; }

[[ -f run_test_all.sh ]] || { echo "error: missing script 'run_test_all.sh'"; exit 255; }

echo "-------------------"
echo "--- START TESTS ---"
echo "-------------------"
./run_test_all.sh

